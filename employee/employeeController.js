var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
var Employee = require('./employee');

router.post('/', async(req, res) => {

    var emailSplitted = req.body.email.split('@')
    var existEmail = null
    try {
        existEmail = await Employee.findOne({ email: req.body.email })
    } catch (error) {
        console.log("Error on get Email")
    }

    if (existEmail) {
        for (var i = 1; i < 100; i++) {
            let emailPermutatted = emailSplitted[0] + '.' + i + '@' + emailSplitted[1]

            let posibleEmail = await Employee.findOne({ email: emailPermutatted })

            if (!posibleEmail) {
                req.body.email = emailPermutatted
                break
            }
        }
    }

    Employee.create({
            firstLastName: req.body.firstLastName,
            secondLastName: req.body.secondLastName,
            firstName: req.body.firstName,
            otherNames: req.body.otherNames,
            country: req.body.country,
            documentType: req.body.documentType,
            documentId: req.body.documentId,
            email: req.body.email,
            admissionDate: req.body.admissionDate,
            area: req.body.area,
            state: req.body.state,
            registryDate: req.body.registryDate,
        },
        function(err, user) {
            if (err) return res.status(500).send('There was a problem adding the information to the database.');
            res.status(200).send(user);
        }
    );
});

router.get('/', async(req, res) => {
    const { page = 1, results = 10 } = req.query;

    try {
        var employees = await Employee.find()
            .limit(results * 1)
            .skip((page - 1) * results)
            .exec();

        const count = await Employee.countDocuments();

        res.json({
            info: {
                page: parseInt(page),
                results: parseInt(results),
                totalCount: parseInt(count)

            },
            results: employees,

        });
    } catch (err) {
        console.error(err.message);
    }
});

router.get('/:id', function(req, res) {
    Employee.findById(req.params.id, function(err, user) {
        if (err) return res.status(500).send('There was a problem finding the user.');
        if (!user) return res.status(404).send('No user found.');
        res.status(200).send(user);
    });
});

router.delete('/:id', function(req, res) {
    Employee.findByIdAndRemove(req.params.id, function(err, user) {
        if (err) return res.status(500).send('There was a problem deleting the user.');
        res.status(200).send('User: ' + user.name + ' was deleted.');
    });
});

router.put('/:id', function(req, res) {
    Employee.findByIdAndUpdate(req.params.id, req.body, { new: true }, function(err, user) {
        if (err) return res.status(500).send('There was a problem updating the user.');
        res.status(200).send(user);
    });
});

router.get('/findByEmail/:email', function(req, res) {
    Employee.findOne({ email: req.params.email }, function(err, employee) {
        res.status(200).send(employee);
    })
})


module.exports = router;