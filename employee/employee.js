var mongoose = require('mongoose');
var EmployeeSchema = new mongoose.Schema({
    firstLastName: String,
    secondLastName: String,
    firstName: String,
    otherNames: String,
    country: String,
    documentType: String,
    documentId: String,
    email: String,
    admissionDate: Date,
    area: String,
    state: String,
    registryDate: Date
});
mongoose.model('Employee', EmployeeSchema);

module.exports = mongoose.model('Employee');