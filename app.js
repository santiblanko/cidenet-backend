var express = require('express');
var app = express();
var db = require('./db');

var cors = require('cors')
app.use(cors())

var UserController = require('./user/UserController');
var EmployeeController = require('./employee/employeeController');

app.use('/users', UserController);
app.use('/employees', EmployeeController);

module.exports = app;